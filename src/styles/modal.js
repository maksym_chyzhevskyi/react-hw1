import styled from "styled-components";

export const MyModalWrapper = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  background: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
`;

MyModalWrapper.displayName = "Modal-wrapper";

export const MyModal = styled.div`
  background-color: ${({ className }) => {
    switch (className) {
      case "red":
        return "#e74c3c";
      case "green":
        return "#36853e";
      case "blue":
        return "#5661e1";
      default:
        return "#e74c3c";
    }
  }};
  border-radius: 5px;
  min-width: 600px;
  color: #fff;
`;

MyModal.displayName = "Modal";

export const MyModalHeader = styled.div`
  display: flex;
  justify-content: space-between;
  border-radius: 5px;
  background-color: ${({ className }) => {
    switch (className) {
      case "red":
        return "#d44637";
      case "green":
        return "#2d8c37";
      case "blue":
        return "#424bb2";
      default:
        return "#d44637";
    }
  }};
`;

export const MyModalCloseBtn = styled.div`
  padding: 15px;
  cursor: pointer;
`;

export const MyModalContent = styled.div`
  padding: 25px;
  text-align: center;
  font-size: 20px;
`;

export const MyModalAction = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 14px;
  padding: 5px 15px 20px;
`;

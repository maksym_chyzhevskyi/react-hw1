import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    body {
    margin-top: 40vh;
    box-sizing: border-box;    
    }
    
    h1 {
    margin: 0;
    padding: 15px;
    font-size: 16px;
    }
    
    p {
    margin: 10px 0;
    }`;

export const AppContainer = styled.div`
  padding: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 14px;
`;

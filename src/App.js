import React, { Component } from "react";
import { Button } from "./components/button";
import { Modal } from "./components/modal";
import { AppContainer, GlobalStyle } from "./styles/style.js";

export class App extends Component {
  constructor() {
    super();

    this.state = {
      modalIsVisible1: false,
      modalIsVisible2: false,
    };
  }

  render() {
    return (
      <AppContainer>
        <GlobalStyle />
        <Button
          className={"red"}
          text="Open first modal window"
          backgroundColor="#b3382c"
          onClick={() => this.setState({ modalIsVisible1: true })}
        />
        {this.state.modalIsVisible1 && (
          <Modal
            className={"red"}
            setVisible={() => this.setState({ modalIsVisible1: false })}
            header={<>Do you want to delete this file?</>}
            closeButton={true}
            text={
              <>
                <p>
                  Once you delete this file, it won't be possible to undo this
                  action.
                </p>
                <p>Are you sure you want to delete it?</p>
              </>
            }
            action={
              <>
                <Button
                  className={"red"}
                  text="OK"
                  backgroundColor="#b3382c"
                  onClick={() => alert("OK")}
                />
                <Button
                  className={"red"}
                  text="Cancel"
                  backgroundColor="#b3382c"
                  onClick={() => this.setState({ modalIsVisible1: false })}
                />
              </>
            }
          />
        )}

        <Button
          className={"green"}
          text="Open second modal window"
          backgroundColor="#34ab40"
          onClick={() => this.setState({ modalIsVisible2: true })}
        />
        {this.state.modalIsVisible2 && (
          <Modal
            className={"green"}
            setVisible={() => this.setState({ modalIsVisible2: false })}
            header={<> Second modal window</>}
            closeButton={true}
            text={
              <>
                <p>HELLO</p>
              </>
            }
            action={
              <>
                <Button
                  className={"green"}
                  text="Submit"
                  backgroundColor="#34ab40"
                  onClick={() => this.setState({ modalIsVisible2: false })}
                />
              </>
            }
          />
        )}
      </AppContainer>
    );
  }
}
